/*
 * Copyright (C) the Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.contextplugins.speechtotext;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.ambientdynamix.api.application.ErrorCodes;
import org.ambientdynamix.api.contextplugin.ContextPluginRuntime;
import org.ambientdynamix.api.contextplugin.ContextPluginSettings;
import org.ambientdynamix.api.contextplugin.PowerScheme;
import org.ambientdynamix.api.contextplugin.security.Permissions;
import org.ambientdynamix.api.contextplugin.security.SecuredSpeechRecognizer;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.util.Log;

/**
 * A speech-to-text plug-in that provides management for Android's SpeechRecognizer facilities.
 * 
 * @author Darren Carlson
 * 
 */
public class SpeechToTextRuntime extends ContextPluginRuntime implements RecognitionListener {
	private final String TAG = this.getClass().getSimpleName();
	private SecuredSpeechRecognizer recognizer;
	private volatile UUID currentRequestId;

	@Override
	public void init(PowerScheme scheme, ContextPluginSettings settings) throws Exception {
		// Store the incoming settings
		this.setPowerScheme(scheme);
		Context c = getPluginFacade().getSecuredContext(getSessionId());
		recognizer = (SecuredSpeechRecognizer) c.getSystemService(Permissions.SECURED_SPEECH_RECOGNIZER);
		if (recognizer != null) {
			Log.i(TAG, "Init got SpeechRecognizer: " + recognizer);
			recognizer.setRecognitionListener(this);
			return;
		}
		Log.i(TAG, "Could not create SpeechRecognizer");
		throw new Exception("Could not create SpeechRecognizer");
	}

	@Override
	public void start() {
		/*
		 * Nothing to do, since this is a pull plug-in... we're now waiting for context scan requests.
		 */
		Log.i(TAG, "Started!");
	}

	@Override
	public void stop() {
		/*
		 * At this point, the plug-in should cancel any ongoing context scans, if there are any.
		 */
		Log.i(TAG, "Stopped!");
		cancel();
	}

	@Override
	public void destroy() {
		/*
		 * At this point, the plug-in should release any resources.
		 */
		stop();
		if (recognizer != null)
			recognizer.destroy();
		Log.i(TAG, "Destroyed!");
	}

	@Override
	public void updateSettings(ContextPluginSettings settings) {
		// Not supported
	}

	public void setPowerScheme(PowerScheme scheme) {
	}

	@Override
	public synchronized void handleContextRequest(UUID requestId, String contextInfoType) {
		if (contextInfoType != null
				&& contextInfoType.equalsIgnoreCase("org.ambientdynamix.contextplugins.speechtotext.voiceresults")) {
			if (currentRequestId == null) {
				currentRequestId = requestId;
				Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
				intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
				intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE,
						"org.ambientdynamix.contextplugins.speechtotext");
				recognizer.startListening(intent);
				Log.i(TAG, "Started translating voice to text");
			} else
				sendContextRequestError(requestId, "Busy", ErrorCodes.RESOURCE_BUSY);
		} else
			sendContextRequestError(requestId, "Context Type Not Supported: " + contextInfoType,
					ErrorCodes.CONTEXT_TYPE_NOT_SUPPORTED);
	}

	@Override
	public synchronized void handleConfiguredContextRequest(UUID requestId, String contextInfoType, Bundle scanConfig) {
		Log.w(TAG, "Configured context scans not supported... ignoring configuration data");
		handleContextRequest(requestId, contextInfoType);
	}

	@Override
	public void onBeginningOfSpeech() {
		Log.i(TAG, "Beginning of speech detected");
	}

	@Override
	public void onBufferReceived(byte[] arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onEndOfSpeech() {
		Log.i(TAG, "End of speech detected");
	}

	// Errors from Android source-code for the SpeechRecognizer
	// /** Network operation timed out. */
	// public static final int ERROR_NETWORK_TIMEOUT = 1;
	//
	// /** Other network related errors. */
	// public static final int ERROR_NETWORK = 2;
	//
	// /** Audio recording error. */
	// public static final int ERROR_AUDIO = 3;
	//
	// /** Server sends error status. */
	// public static final int ERROR_SERVER = 4;
	//
	// /** Other client side errors. */
	// public static final int ERROR_CLIENT = 5;
	//
	// /** No speech input */
	// public static final int ERROR_SPEECH_TIMEOUT = 6;
	//
	// /** No recognition result matched. */
	// public static final int ERROR_NO_MATCH = 7;
	//
	// /** RecognitionService busy. */
	// public static final int ERROR_RECOGNIZER_BUSY = 8;
	//
	// /** Insufficient permissions */
	// public static final int ERROR_INSUFFICIENT_PERMISSIONS = 9;
	@Override
	public void onError(int error) {
		if (currentRequestId != null) {
			if (error == SpeechRecognizer.ERROR_RECOGNIZER_BUSY) {
				sendContextRequestError(currentRequestId, "SpeechRecognizer error, code: " + error,
						ErrorCodes.RESOURCE_BUSY);
			} else
				sendContextRequestError(currentRequestId, "SpeechRecognizer error, code: " + error,
						ErrorCodes.REQUEST_FAILED);
			cancel();
		} else
			Log.w(TAG, "Got speech recognition onError while not processing: " + error);
	}

	@Override
	public void onEvent(int eventType, Bundle params) {
		// TODO Auto-generated method stub
	}

	@Override
	public synchronized void onPartialResults(Bundle partialResults) {
		Log.i(TAG, "Got partial results");
	}

	@Override
	public void onReadyForSpeech(Bundle params) {
		Log.i(TAG, "onReadyForSpeech");
	}

	@Override
	public synchronized void onResults(Bundle results) {
		if (currentRequestId != null) {
			List<String> tmp = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
			String[] voiceArray = (String[]) tmp.toArray(new String[tmp.size()]);
			float[] scoreArray = results.getFloatArray("confidence_scores");
			List<IVoiceResult> voiceResults = new ArrayList<IVoiceResult>();
			for (int i = 0; i < voiceArray.length; i++) {
				if (scoreArray != null && scoreArray.length > 0)
					voiceResults.add(new VoiceResult(voiceArray[i], scoreArray[i]));
				else
					voiceResults.add(new VoiceResult(voiceArray[i], 0));
			}
			sendContextEvent(currentRequestId, new VoiceResults(voiceResults));
			cancel();
		} else
			Log.w(TAG, "Got speech recognition results while not processing!");
	}

	@Override
	public void onRmsChanged(float rmsdB) {
		// TODO Auto-generated method stub
	}

	private void cancel() {
		if (recognizer != null)
			recognizer.cancel();
		currentRequestId = null;
	}
}